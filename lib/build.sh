build_failed() {
  head "Build failed"
  echo ""
  cat $warnings | indent
  info "We're sorry, this build is failing!"
}

build_succeeded() {
  head "Build succeeded!"
  echo ""
  (bower list || true) 2>/dev/null | indent
  cat $warnings | indent
  echo ""
}

get_components_source() {
  local build_dir=$1
  if test -f $build_dir/bower.json; then
    echo "bower.json"
  else
    echo ""
  fi
}

get_components_cached() {
  local cache_dir=$1
  if test -d $cache_dir/bower/bower_components; then
    echo "true"
  else
    echo "false"
  fi
}

# Sets:
# components_source
# bower_components
# bower_previous
# components_cached
# environment variables (from ENV_DIR)

read_current_state() {
  info "bower.json..."
  assert_json "$build_dir/bower.json"

  info "build directory..."
  components_source=$(get_components_source "$build_dir")

  if test -f $build_dir/.bowerrc; then
    assert_json "$build_dir/.bowerrc"
    bower_components=$(read_json "$build_dir/.bowerrc" ".directory")
    if [ "$bower_components" == "" ]; then
      bower_components="bower_components"
    fi
  fi

  info "cache directory..."
  bower_previous=$(file_contents "$cache_dir/bower/bower-version")
  components_cached=$(get_components_cached "$cache_dir")

  info "environment variables..."
  export_env_dir $env_dir
  export BOWER_COMPONENTS_CACHE=${BOWER_COMPONENTS_CACHE:-true}
}

show_current_state() {
  echo ""
  info "bower_components source: ${components_source:-none}"
  info "bower_components cached: $components_cached"
  echo ""

  info "BOWER_COMPONENTS_CACHE=$BOWER_COMPONENTS_CACHE"
  info "Using bower version: `bower --version`"
}

function build_dependencies() {
  if [ "$components_source" == "" ]; then
    info "Skipping dependencies (no source for bower_components)"

  else
    cache_status=$(get_cache_status)

    if [ "$cache_status" == "valid" ]; then
      info "Restoring bower components from cache"
      cp -r $cache_dir/bower/bower_components $build_dir/
      info "Pruning unused dependencies"
      bower prune --production --quiet 2>&1 | indent
      info "Installing any new components"
      bower install --production --quiet 2>&1 | indent
    else
      info "$cache_status"
      info "Installing bower components"
      touch $build_dir/.bowerrc
      bower install --production --quiet 2>&1 | indent
    fi
  fi
}

clean_bower() {
  info "Cleaning bower artifacts"
  bower cache clean --quiet
}

# Caching

create_cache() {
  info "Caching results for future builds"
  mkdir -p $cache_dir/bower

  echo `bower --version` > $cache_dir/bower/bower-version

  if test -d $build_dir/$bower_components; then
    cp -r $build_dir/$bower_components $cache_dir/bower
  fi
}

clean_cache() {
  info "Cleaning previous cache"
  rm -rf "$cache_dir/bower"
}

get_cache_status() {
  local bower_version=`bower --version`

  # Did we bust the cache?
  if ! $components_cached; then
    echo "No cache available"
  elif ! $BOWER_COMPONENTS_CACHE; then
    echo "Cache disabled with BOWER_COMPONENTS_CACHE"
  elif [ "$bower_previous" != "" ] && [ "$bower_version" != "$bower_previous" ]; then
    echo "Bower version changed ($bower_previous => $bower_version); invalidating cache"
  else
    echo "valid"
  fi
}
