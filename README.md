# Heroku Buildpack for Bower

A build pack to install and cache bower_components, like heroku-buildpack-nodejs does for node_modules.

If you fork this repository, please **update this README** to explain what your fork does and why it's special.


## How it Works

Apps are built via one of two paths:

1. A regular `bower install` (first build; default scenario)
2. Copy existing `bower_components` from cache, then `bower prune`, then `bower install` (subsequent builds)

For technical details, check out the heavily-commented compile script.

## Documentation

For more information about using Node.js and buildpacks on Heroku, see these Dev Center articles:

- [Heroku Node.js Support](https://devcenter.heroku.com/articles/nodejs-support)
- [Getting Started with Node.js on Heroku](https://devcenter.heroku.com/articles/nodejs)
- [Buildpacks](https://devcenter.heroku.com/articles/buildpacks)
- [Buildpack API](https://devcenter.heroku.com/articles/buildpack-api)


## Options

### Enable or disable bower_components caching

For a 'clean' build without using any cached node modules:

```shell
heroku config:set BOWER_COMPONENTS_CACHE=false
git commit -am 'rebuild' --allow-empty
git push heroku master
heroku config:unset BOWER_COMPONENTS_CACHE
```

Caching bower_components between builds dramatically speeds up build times.
However, `bower install` doesn't automatically update already-installed
components as long as they fall within acceptable semver ranges,
which can lead to outdated modules.

Default: `BOWER_COMPONENTS_CACHE` defaults to true

### Configure bower with .bowerrc

Sometimes, a project needs custom bower behavior. For any such behavior,
just include an `.bowerrc` file in the root of your project.
